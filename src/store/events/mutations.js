export const setEvents = (state, events) => {
  state.events = [...events]
}

export const setEvent = (state, event) => {
  state.event = { ...event }
}
