export const setRoles = (state, roles) => {
  state.roles = [...roles]
}

export const setRole = (state, role) => {
  state.role = { ...role }
}
