export const setUser = (state, user) => {
  state.user = { ...user }
}

export const setLoggedIn = (state, loggedIn) => {
  state.loggedIn = loggedIn
}
