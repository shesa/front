export const setExpenditures = (state, expenditures) => {
  state.expenditures = [...expenditures]
}

export const setExpenditure = (state, expenditure) => {
  state.expenditure = { ...expenditure }
}
