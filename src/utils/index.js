import { Plugins } from '@capacitor/core'

const { Storage } = Plugins

export const chunk = (arr, size) => (
  Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
    arr.slice(i * size, i * size + size)
  )
)

export const getErrorMsgFromResponse = err => {
  if (err.response && err.response.data && err.response.data.message) {
    return err.response.data.message
  }

  return err.message
}

export const isExcoMember = async () => {
  const { value } = await Storage.get({ key: process.env.STORAGE_USER_KEY })

  if (!value) return false

  const user = JSON.parse(value)

  return user.role && (user.role.name !== 'member')
}

export const canEditFinances = async () => {
  const { value } = await Storage.get({ key: process.env.STORAGE_USER_KEY })

  if (!value) return false

  const user = JSON.parse(value)

  return user.role && (
    user.role.name === 'president' || user.role.name === 'vice-president'
    || user.role.name === 'treasurer' || user.role.name === 'financial-secretary'
  )
}
