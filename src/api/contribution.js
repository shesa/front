import { api } from 'boot/axios'

const endpoint = '/contributions'

export const getContributions = params => {
  return api.get(`${endpoint}`, { params })
}

export const getContribution = id => {
  return api.get(`${endpoint}/${id}`)
}

export const createContribution = data => {
  return api.post(`${endpoint}`, data)
}

export const updateContribution = (id, data) => {
  return api.put(`${endpoint}/${id}`, data)
}

export const deleteContribution = id => {
  return api.delete(`${endpoint}/${id}`)
}
