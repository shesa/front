import { api } from 'boot/axios'

const endpoint = '/expenditures'

export const getExpenditures = params => {
  return api.get(endpoint, { params })
}

export const getExpenditure = id => {
  return api.get(`${endpoint}/${id}`)
}

export const getEventExpenditure = eventId => {
  return api.get(`event-expenditure/${eventId}`)
}

export const createExpenditure = data => {
  return api.post(endpoint, data)
}

export const updateExpenditure = (id, data) => {
  return api.put(`${endpoint}/${id}`, data)
}

export const deleteExpenditure = id => {
  return api.delete(`${endpoint}/${id}`)
}
