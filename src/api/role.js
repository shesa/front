import { api } from 'boot/axios'

const endpoint = '/roles'

export const getRoles = params => {
  return api.get(`${endpoint}`, { params })
}

export const getRole = id => {
  return api.get(`${endpoint}/${id}`)
}

export const createRole = data => {
  return api.post(`${endpoint}`, data)
}

export const updateRole = (id, data) => {
  return api.put(`${endpoint}/${id}`, data)
}

export const deleteRole = id => {
  return api.delete(`${endpoint}/${id}`)
}
