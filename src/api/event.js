import { api } from 'boot/axios'

const endpoint = '/events'

export const getEvents = params => {
  return api.get(`${endpoint}`, { params })
}

export const getEvent = id => {
  return api.get(`${endpoint}/${id}`)
}

export const createEvent = data => {
  return api.post(`${endpoint}`, data)
}

export const updateEvent = (id, data) => {
  return api.put(`${endpoint}/${id}`, data)
}

export const deleteEvent = id => {
  return api.delete(`${endpoint}/${id}`)
}
