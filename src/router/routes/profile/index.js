const profileRoutes = [
  {
    path: '/profile',
    component: () => import('layouts/ProfileLayout.vue'),
    children: [
      { path: '', name: 'Profile', component: () => import('pages/Profile/Index.vue') },
      { path: 'update-password', name: 'UpdatePassword', component: () => import('pages/Profile/UpdatePassword.vue') }
    ]
  }
]

export default profileRoutes
