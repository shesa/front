const appRoutes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'Home', component: () => import('pages/App/Index.vue') },
      {
        path: 'roles',
        component: () => import('layouts/DefaultLayout.vue'),
        children: [
          { path: '', name: 'Roles', component: () => import('pages/App/Roles/Index.vue') },
          {
            path: 'create',
            name: 'CreateRole',
            component: () => import('pages/App/Roles/Create.vue'),
            meta: {
              requiresExco: true
            }
          },
          {
            path: ':id/edit',
            name: 'EditRole',
            component: () => import('pages/App/Roles/Edit.vue'),
            meta: {
              requiresExco: true
            }
          }
        ]
      },
      {
        path: 'members',
        component: () => import('layouts/DefaultLayout.vue'),
        children: [
          { path: '', name: 'Members', component: () => import('pages/App/Members/Index.vue') },
          {
            path: 'create',
            name: 'CreateMember',
            component: () => import('pages/App/Members/Create.vue'),
            meta: {
              requiresExco: true
            }
          },
          {
            path: ':id/edit',
            name: 'EditMember',
            component: () => import('pages/App/Members/Edit.vue'),
            meta: {
              requiresExco: true
            }
          }
        ]
      },
      {
        path: 'events',
        component: () => import('layouts/DefaultLayout.vue'),
        children: [
          { path: '', name: 'Events', component: () => import('pages/App/Events/Index.vue') },
          {
            path: 'create',
            name: 'CreateEvent',
            component: () => import('pages/App/Events/Create.vue'),
            meta: {
              requiresExco: true
            }
          },
          {
            path: ':id/contributions',
            name: 'EventContributions',
            component: () => import('pages/App/Events/Contributions.vue')
          },
          {
            path: ':id/edit',
            name: 'EditEvent',
            component: () => import('pages/App/Events/Edit.vue'),
            meta: {
              requiresExco: true
            }
          }
        ]
      },
      { path: 'event-quotas', name: 'EventQuotas', component: () => import('pages/App/EventQuotas/Index.vue') },
      {
        path: 'expenditures',
        component: () => import('layouts/DefaultLayout.vue'),
        children: [
          { path: '', name: 'Expenditures', component: () => import('pages/App/Expenditures/Index.vue') },
          {
            path: 'create',
            name: 'CreateExpenditure',
            component: () => import('pages/App/Expenditures/Create.vue'),
            meta: {
              requiresExco: true
            }
          },
          {
            path: ':id/edit',
            name: 'EditExpenditure',
            component: () => import('pages/App/Expenditures/Edit.vue'),
            meta: {
              requiresExco: true
            }
          }
        ]
      },
      { path: 'profile', name: 'Profile', component: () => import('pages/Profile/Index.vue') },
      { path: 'update-password', name: 'UpdatePassword', component: () => import('pages/Profile/UpdatePassword.vue') }
    ]
  }
]

export default appRoutes
