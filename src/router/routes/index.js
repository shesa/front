// Routes
import authRoutes from './auth'
// import profileRoutes from './profile'
import appRoutes from './app'

const routes = [
  ...authRoutes,
  // ...profileRoutes,
  ...appRoutes,

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    name: 'page404',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
